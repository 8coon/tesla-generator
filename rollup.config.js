const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');

module.exports = {
	input: 'src/index.js',
	output: {
		file: 'lib/tesla.js',
		format: 'umd',
		name: 'tesla',
	},
	plugins: [
		babel(),
		resolve(),
	],
};
