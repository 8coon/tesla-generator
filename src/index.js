const EventBus = require('./event-bus');
const effects = require('./effects');

class Tesla {
	constructor() {
		this._eventBus = new EventBus();
	}

	run(generatorFn, event = null) {
		this._eventBus.run(generatorFn, event);
	}

	dispatch(event) {
		this._eventBus.dispatch(event);
	}
}

module.exports = {
	...effects,
	Tesla,
};
