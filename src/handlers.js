const Filter = require('./filter');
const {EFFECT_TAKE_EVERY, EFFECT_TAKE, EFFECT_RACE} = require('./effects');

const handlers = {
	/**
	 * @param {EventBus} eventBus
	 * @param effect
	 * @param {Wrapper} generator
	 * @param {Filter[]} [filters]
	 * @param {Function} [onMatch]
	 * @return {boolean}
	 */
	[EFFECT_TAKE_EVERY](eventBus, effect, generator, filters = null, onMatch = null) {
		let {matcher, generatorFunction} = effect;

		if (!Array.isArray(matcher)) {
			matcher = [matcher];
		}

		// On any match run the handler generator
		const handleTakeEveryMatch = event => {
			eventBus.run(generatorFunction, event);
			// Call the callback
			onMatch && onMatch(event);
		};

		let filter;
		let i = matcher.length;

		while (i--) {
			// Create a Filter from a matcher
			filter = new Filter(matcher[i], handleTakeEveryMatch);
			// Register in the global scope
			eventBus.addFilter(filter);
			// Store it to delete sometimes
			filters && filters.push(filter);
		}

		// We should continue generator execution
		return !onMatch && true;
	},

	/**
	 * @param {EventBus} eventBus
	 * @param effect
	 * @param {Wrapper} generator
	 * @param {Filter[]} [filters]
	 * @param {Function} [onMatch]
	 * @return {boolean}
	 */
	[EFFECT_TAKE](eventBus, effect, generator, filters = null, onMatch = null) {
		let {matcher} = effect;

		if (!Array.isArray(matcher)) {
			matcher = [matcher];
		}

		// Storing all created filters
		filters = filters || [];

		// On the first successful match will delete all installed filters
		const handleTakeMatch = event => {
			let i = filters.length;

			while (i--) {
				eventBus.removeFilter(filters[i]);
			}

			if (!onMatch) {
				// Run the generator
				eventBus.continueRunning(generator, event);
			} else {
				// Call the callback
				onMatch(event);
			}
		};

		let filter;
		let i = matcher.length;

		while (i--) {
			// Create a Filter from a matcher
			filter = new Filter(matcher[i], handleTakeMatch);
			// Store it to delete when a Filter matches
			filters.push(filter);
			// Register in the global scope
			eventBus.addFilter(filter);
		}

		// We should pause generator execution
		return false;
	},

	/**
	 * @param {EventBus} eventBus
	 * @param effect
	 * @param {Wrapper} generator
	 * @param {Filter[]} [filters]
	 * @param {Function} [onMatch]
	 */
	[EFFECT_RACE](eventBus, effect, generator, filters = null, onMatch = null) {
		const {matchers} = effect;
		const typeofMatchers = typeof matchers;
		const isArray = Array.isArray(matchers);
		const isObject = !isArray && typeofMatchers === 'object';

		// Check for correct matchers type
		if (!isArray && !isObject) {
			throw new Error(`Expected race matchers to be array or object, but got "${typeofMatchers}"`);
		}

		let result;

		if (isArray) {
			// Creating result array
			result = [];
			result.length = matchers.length;
		} else {
			// Creating result map
			result = {};
		}

		// Scope and iterator variables
		const childFilters = [];

		// Matcher values and keys (for object only)
		let matchersKeys = [];
		let matchersValues = matchers;

		if (isObject) {
			matchersValues = [];

			// Iterate through object fields, save keys and values
			for (let key in matchers) {
				if (matchers.hasOwnProperty(key)) {
					matchersKeys.push(key);
					matchersValues.push(matchers[key]);
				}
			}
		}

		let i = matchersValues.length;

		// Iterate through matchers
		while (i--) {
			const childEffect = matchersValues[i];
			const childEffectKey = isArray ? i : matchersKeys[i];

			// Child effect has a wrong type, do nothing
			if (!childEffect || typeof childEffect !== 'object') {
				continue;
			}

			// Get child effect implementation
			const childEffectHandler = handlers[childEffect.type];

			if (!childEffectHandler) {
				continue;
			}

			// Call it, passing stored filters and onMatch
			childEffectHandler(eventBus, childEffect, generator, childFilters, event => {
				// Setting value in result object/array
				result[childEffectKey] = event;

				let i = childFilters.length;
				// Removing all stored filters
				while (i--) {
					eventBus.removeFilter(childFilters[i]);
				}

				// Resuming generator with race effect result
				eventBus.continueRunning(generator, result);
			});
		}

		// We should pause generator execution
		return false;
	}
};

module.exports = handlers;
