
const TYPE_STRING = 1;
const TYPE_REGEXP = 2;
const TYPE_FUNCTION = 3;
const TYPE_CONST = 4;

const ALL_SELECTOR = '*';

/**
 * @typedef {Object} Filter
 *
 * @property {string} key
 * @property {function} handleMatch
 */
class Filter {

	/**
	 * @param {string|RegExp|function} matcher
	 * @param {function} onMatchCb
	 * @memberOf Filter
	 * @constructor
	 */
	constructor(matcher, onMatchCb) {
		this.handleMatch = onMatchCb;
		this.key = null;
		this._matcher = matcher;

		const type = typeof matcher;

		switch (type) {
			case 'string': {
				if (matcher === ALL_SELECTOR) {
					this._matcherType = TYPE_CONST;
					return;
				}

				this._matcherType = TYPE_STRING;
				this.key = matcher;
				return;
			}

			case 'function': {
				this._matcherType = TYPE_FUNCTION;
				return;
			}

			default: {
				if (!matcher) {
					throw new Error('Expected matcher to be defined');
				}

				if (matcher instanceof RegExp) {
					this._matcherType = TYPE_REGEXP;
					return;
				}
			}
		}

		throw new Error(
			`Expected matcher to be a string, function or regular expression, but received "${type}"`
		);
	}

	/**
	 * @param event
	 * @memberOf Filter
	 * @return {boolean}
	 */
	test(event) {
		const type = event.type;

		switch (this._matcherType) {
			case TYPE_CONST: return true;
			case TYPE_STRING: return this._matcher === type;
			case TYPE_REGEXP: return this._matcher.test(type);
			case TYPE_FUNCTION: return this._matcher(event);
		}
	}

}

module.exports = Filter;
