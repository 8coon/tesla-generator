
const EFFECT_TAKE_EVERY = 'takeEvery';
const EFFECT_TAKE = 'take';
const EFFECT_RACE = 'race';
const EFFECT_FORK = 'fork';
const EFFECT_ALL = 'all';

module.exports = {
	EFFECT_TAKE_EVERY,
	EFFECT_TAKE,
	EFFECT_RACE,
	EFFECT_FORK,
	EFFECT_ALL,

	/**
	 * @param matcher
	 * @param generatorFunction
	 */
	takeEvery(matcher, generatorFunction) {
		return {
			type: EFFECT_TAKE_EVERY,
			generatorFunction,
			matcher,
		};
	},

	/**
	 * @param matcher
	 */
	take(matcher) {
		return {
			type: EFFECT_TAKE,
			matcher
		};
	},

	/**
	 * @param matchers
	 */
	race(matchers) {
		return {
			type: EFFECT_RACE,
			matchers
		};
	},

	/**
	 * @param generatorFunction
	 * @param args
	 */
	fork(generatorFunction, ...args) {
		return {
			type: EFFECT_FORK,
			arguments: args,
			generatorFunction,
		};
	},

	/**
	 * @param matchers
	 */
	all(matchers) {
		return {
			type: EFFECT_ALL,
			matchers
		};
	},

};
