
/**
 * @typedef {Object} Wrapper
 *
 * @property {boolean} destroyed
 */
class Wrapper {

	/**
	 * @param {GeneratorFunction} generatorFn
	 * @param [event]
	 * @memberOf Generator
	 * @constructor
	 */
	constructor(generatorFn, event = null) {
		this._generator = generatorFn(event);
	}

	/**
	 * @param event
	 * @methodOf Wrapper
	 */
	next(event) {
		const generatorResult = this._generator.next(event);

		if (generatorResult.done) {
			this.destroyed = true;
		}

		return generatorResult.value;
	}
}

module.exports = Wrapper;
