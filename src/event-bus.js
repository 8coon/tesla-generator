const Wrapper = require('./wrapper');
const Filter = require('./filter');
const handlers = require('./handlers');

function checkDefinedType(argName, typeName, arg) {
	if (!arg) {
		throw new Error(`Expected ${argName} to be defined`);
	}

	const type = typeof arg;

	if (type !== typeName) {
		throw new Error(`Expected ${argName} to be "${typeName}", but received "${type}"`);
	}
}

/**
 * @typedef {Object} EventBus
 */
class EventBus {
	/**
	 * @memberOf EventBus
	 * @constructor
	 */
	constructor() {
		this._filters = [];
		this._filtersByKey = {};
	}

	/**
	 * @param event
	 * @memberOf EventBus
	 */
	dispatch(event) {
		checkDefinedType('event', 'object', event);

		const {type} = event;
		const filtersByKey = this._filtersByKey[type];
		const matchedFilters = [];
		let filter;

		// Check for filters by key
		if (filtersByKey) {
			// These filters always match
			matchedFilters.push.apply(matchedFilters, filtersByKey);
		}

		let i = this._filters.length;

		// Check for all other filters
		while (i--) {
			filter = this._filters[i];

			// If Filter matches
			if (filter.test(event)) {
				matchedFilters.push(filter);
			}
		}

		i = matchedFilters.length;

		// Running filter matches
		while (i--) {
			matchedFilters[i].handleMatch(event);
		}
	}

	/**
	 * @param {function} generatorFn
	 * @param [event]
	 * @memberOf EventBus
	 */
	run(generatorFn, event = null) {
		checkDefinedType('generatorFn', 'function', generatorFn);

		const generator = new Wrapper(generatorFn);
		this.continueRunning(generator, event);
	}

	/**
	 * @param {Wrapper} generator
	 * @param [event]
	 * @memberOf EventBus
	 */
	continueRunning(generator, event = null) {
		const effect = generator.next(event);
		const typeofEffect = typeof effect;

		const effectHandler = effect &&
			typeofEffect === 'object' &&
			handlers[effect.type];

		// If a generator yielded nothing
		if (!effectHandler) {
			// If it is not destroyed, we need to run it again
			if (!generator.destroyed) {
				this.continueRunning(generator);
			}

			return;
		}

		// Handle effect by type
		const shouldContinue = effectHandler(this, effect, generator);

		// If we need to continue running this generator
		if (shouldContinue && !generator.destroyed) {
			this.continueRunning(generator);
		}
	}

	/**
	 * @param {Filter} filter
	 * @memberOf EventBus
	 */
	addFilter(filter) {
		const filterKey = filter.key;

		if (filterKey !== null) {
			// Filter has a key, we store it in a map
			let filtersByKey = this._filtersByKey[filterKey];

			if (!filtersByKey) {
				filtersByKey = this._filtersByKey[filterKey] = [];
			}

			filtersByKey.push(filter);
		} else {
			// Otherwise store it in an array
			this._filters.push(filter);
		}
	}

	/**
	 * @param {Filter} filter
	 * @memberOf EventBus
	 */
	removeFilter(filter) {
		const filterKey = filter.key;
		let i;

		if (filterKey !== null) {
			// Filter has a key, we need to delete it from a map
			const filtersByKey = this._filtersByKey[filterKey];

			if (!filtersByKey) {
				return;
			}

			// Searching for this filter in the global scope by key
			for (i = 0; i < filtersByKey.length; i++) {
				if (filtersByKey[i] === filter) {
					// It is the last filter with this key
					if (filtersByKey.length === 1) {
						delete this._filtersByKey[filterKey];
						return;
					}

					filtersByKey.splice(i, 1);
					return;
				}
			}
		} else {
			// Otherwise delete it from an array
			for (i = 0; i < this._filters.length; i++) {
				if (this._filters[i] === filter) {
					this._filters.splice(i, 1);
					return;
				}
			}
		}
	}
}

module.exports = EventBus;
