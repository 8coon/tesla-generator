const Filter = require('../src/filter');
const EventBus = require('../src/event-bus');
const effects = require('../src/effects');

describe('EventBus', () => {
	let eventBus;

	beforeEach(() => {
		eventBus = new EventBus();
	});

	test('.addFilter should store a filter in an array', () => {
		// Expect _filters to be empty by default
		expect(eventBus._filters).toEqual([]);

		const filter = new Filter(/.*/);

		// Adding filter
		eventBus.addFilter(filter);

		// Expect this filter to be added
		expect(eventBus._filters).toEqual([filter]);
	});

	test('.addFilter should store a filter in a map', () => {
		// Expect _filtersByKey to be empty by default
		expect(eventBus._filtersByKey).toEqual({});

		const filterA = new Filter('a');
		const filterB = new Filter('b');

		// Adding filters
		eventBus.addFilter(filterA);
		eventBus.addFilter(filterB);
		eventBus.addFilter(filterA);

		// Expect these filters to be added
		expect(eventBus._filtersByKey).toEqual({
			a: [filterA, filterA],
			b: [filterB],
		});
	});

	test('.removeFilter should remove filter from an array', () => {
		const filter = new Filter(/.*/);

		// Adding filter
		eventBus.addFilter(filter);

		// Removing filter
		eventBus.removeFilter(filter);

		// Expect _filters to be empty
		expect(eventBus._filters).toEqual([]);
	});

	test('.removeFilter should remove filter from a map', () => {
		const filterA = new Filter('a');

		// Adding filters
		eventBus.addFilter(filterA);
		eventBus.addFilter(filterA);

		// Removing filters
		eventBus.removeFilter(filterA);

		// Expect it to be removed
		expect(eventBus._filtersByKey).toEqual({
			a: [filterA],
		});

		eventBus.removeFilter(filterA);

		// Expect _filters to be empty
		expect(eventBus._filtersByKey).toEqual({});
	});

	test('.removeFilter should do nothing if filter was not added', () => {
		const filterA = new Filter('a');
		const filterB = new Filter(/.*/);
		const filterC1 = new Filter('c');
		const filterC2 = new Filter('c');
		const filterD = new Filter(/.*/);

		eventBus.addFilter(filterC1);
		eventBus.addFilter(filterD);

		// Removing filters should not throw
		expect(() => {
			eventBus.removeFilter(filterA);
			eventBus.removeFilter(filterB);
			eventBus.removeFilter(filterC2);
		}).not.toThrow();
	});

	test('.run should not block on a non-blocking effect', () => {
		const fn = jest.fn();

		// Running root generator
		eventBus.run(function *() {
			fn(0);
			yield effects.takeEvery('*', function *() {});

			fn(1);
			yield effects.takeEvery('*', function *() {});
		});

		// Expect takeEvery to be a non-blocking effect
		expect(fn.mock.calls).toEqual([[0], [1]]);
	});

	test('.run should block on a blocking effect', () => {
		const fn = jest.fn();

		// Running root generator
		eventBus.run(function *() {
			fn(0);
			yield effects.take('*');
			fn(1);
		});

		// Expect take to be a non-blocking effect
		expect(fn.mock.calls).toEqual([[0]]);
	});

	test('.run should not block if a generator yielded nothing', () => {
		const fn = jest.fn();

		// Running root generator
		eventBus.run(function *() {
			fn(0);
			yield;
			fn(1);
			yield 'a';
			fn(2);
			yield {type: 'invalid-effect-type'};
			fn(3);
		});

		// Expect to not block on yielding a non-effect
		expect(fn.mock.calls).toEqual([[0], [1], [2], [3]]);
	});

	test('.run should throw when trying to run an invalid generator', () => {
		expect(() => {
			eventBus.run();
		}).toThrowError('Expected generatorFn to be defined');

		expect(() => {
			eventBus.run(123)
		}).toThrowError('Expected generatorFn to be "function", but received "number"');
	});

	test('.dispatch should dispatch event', () => {
		const fn = jest.fn();

		// Root generator
		eventBus.run(function *() {
			let action;
			fn(0);

			action = yield effects.take('event');
			expect(action).toEqual({type: 'event'});
			fn(1);

			action = yield effects.take('*');
			expect(action).toEqual({type: 'event'});
			fn(2);

			action = yield effects.take(/.*/);
			expect(action).toEqual({type: 'event'});
			fn(3);

			action = yield effects.take(_ => _.type === 'event');
			expect(action).toEqual({type: 'event'});
			fn(4);
		});

		// Dispatching test events
		eventBus.dispatch({type: 'event'});
		eventBus.dispatch({type: 'event'});
		eventBus.dispatch({type: 'event'});
		eventBus.dispatch({type: 'event'});

		// Expect generator to be executed
		expect(fn.mock.calls).toEqual([
			[0], [1], [2], [3], [4]
		]);
	});

	test('.dispatch should do nothing if no event matches current filters', () => {
		// Adding event filter
		eventBus.addFilter(new Filter(/^event$/));

		expect(() => {
			// Dispatching event and expecting it not no throw
			eventBus.dispatch({type: 'a'});
		}).not.toThrow();
	});

	test('.dispatch should throw on an erroneous event', () => {
		expect(() => {
			eventBus.dispatch();
		}).toThrowError('Expected event to be defined');

		expect(() => {
			eventBus.dispatch('event');
		}).toThrowError('Expected event to be "object", but received "string"');
	});

});
