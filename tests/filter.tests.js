const Filter = require('../src/filter');

describe('Filter', () => {

	test('Matching with a string', () => {
		const filter = new Filter('event');
		expect(filter.test({type: 'event'})).toBeTruthy();
	});

	test('Matching with a function', () => {
		const matcher = jest.fn(
			event => event.type === 'ok'
		);

		const filter = new Filter(matcher);

		expect(filter.test({type: 'ok'})).toBeTruthy();
		expect(filter.test({type: 'not ok'})).toBeFalsy();

		expect(matcher.mock.calls).toEqual([
			[{type: 'ok'}], [{type: 'not ok'}]
		]);
	});

	test('Matching with a regular expression', () => {
		const filter = new Filter(/^ok/);

		expect(filter.test({type: 'okie'})).toBeTruthy();
		expect(filter.test({type: 'dokie'})).toBeFalsy();
	});

	test('Matching with everything', () => {
		const filter = new Filter('*');

		expect(filter.test({type: 'okie'})).toBeTruthy();
		expect(filter.test({type: 'dokie'})).toBeTruthy();
	});

	test('Saving handleMatch callback', () => {
		const onMatch = _ => _;
		const filter = new Filter('*', onMatch);

		expect(filter.handleMatch).toBe(onMatch);
	});

	test('Throwing on an undefined matcher', () => {
		expect(_ => new Filter()).toThrowError('Expected matcher to be defined');
	});

	test('Throwing on matcher of erroneous type', () => {
		expect(_ => new Filter(12)).toThrowError(
			'Expected matcher to be a string, function or regular expression, but received "number"'
		);

		expect(_ => new Filter({})).toThrowError(
			'Expected matcher to be a string, function or regular expression, but received "object"'
		);
	});

	test('Expect only string Filter to have a key', () => {
		const allFilter = new Filter('*');
		const stringFilter = new Filter('event');
		const regexpFilter = new Filter(/.*/);
		const functionFilter = new Filter(_ => _.type === 'event');

		expect(allFilter.key).toBeNull();
		expect(stringFilter.key).toBe('event');
		expect(regexpFilter.key).toBeNull();
		expect(functionFilter.key).toBeNull();
	});

});
