const effects = require('../src/effects');

describe('effects', () => {

	test('takeEvery', () => {
		const handler = _ => _;
		const effect = effects.takeEvery('*', handler);

		expect(effect).toEqual({
			type: effects.EFFECT_TAKE_EVERY,
			matcher: '*',
			generatorFunction: handler,
		});
	});

	test('take', () => {
		const effect = effects.take('*');

		expect(effect).toEqual({
			type: effects.EFFECT_TAKE,
			matcher: '*',
		});
	});

	test('fork', () => {
		const handler = _ => _;
		const effect = effects.fork(handler, 'a', 'b');

		expect(effect).toEqual({
			type: effects.EFFECT_FORK,
			generatorFunction: handler,
			arguments: ['a', 'b'],
		});
	});

	test.each(['race', 'all'])('%s', (effectName) => {
		const effect = effects[effectName]({
			first: effects.take('first'),
			second: effects.take('second'),
		});

		expect(effect).toEqual({
			type: effectName,
			matchers: {
				first: {
					type: effects.EFFECT_TAKE,
					matcher: 'first',
				},
				second: {
					type: effects.EFFECT_TAKE,
					matcher: 'second',
				}
			}
		});
	});

});
