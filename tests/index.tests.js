const library = require('../src/index');
const {Tesla, take} = library;

describe('index', () => {

	test('Tesla and all effects should be exported', () => {
		expect(library).toEqual(expect.objectContaining({
			Tesla: expect.any(Function),
			takeEvery: expect.any(Function),
			take: expect.any(Function),
			race: expect.any(Function),
			fork: expect.any(Function),
			all: expect.any(Function),
		}));
	});

	test('Tesla.run and Tesla.dispatch', () => {
		const tesla = new Tesla();
		const fn = jest.fn();

		// Root generator
		tesla.run(function *() {
			let event;

			fn(0);
			event = yield take(['first', 'second']);
			expect(event.type).toBe('first');

			fn(1);
			event = yield take(['first', 'second']);
			expect(event.type).toBe('second');

			fn(2);
		});

		// Dispatching events
		tesla.dispatch({type: 'first'});
		tesla.dispatch({type: 'second'});

		expect(fn.mock.calls).toEqual([
			[0], [1], [2],
		]);
	});

});
