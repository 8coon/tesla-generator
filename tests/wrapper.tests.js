const Wrapper = require('../src/wrapper');

describe('Wrapper', () => {

	test('Executes generator function', () => {
		const fn = jest.fn();

		const wrapper = new Wrapper(function *(initial) {
			expect(initial).toBe('initial');
			let event;
			fn(0);

			event = yield 'first';
			// The first value returned by test exists
			expect(event).toBe('first');
			fn(1);

			event = yield 'second';
			// The second does not
			expect(event).toBeUndefined();
			fn(2);
		}, 'initial');

		// No generator is being run in constructor
		expect(fn.mock.calls).toEqual([]);
		expect(wrapper.destroyed).toBeFalsy();

		// Initial
		wrapper.next();
		expect(fn.mock.calls).toEqual([[0]]);
		expect(wrapper.destroyed).toBeFalsy();

		// First run
		wrapper.next('first');
		expect(fn.mock.calls).toEqual([[0], [1]]);
		expect(wrapper.destroyed).toBeFalsy();

		// Second run
		wrapper.next();
		expect(fn.mock.calls).toEqual([[0], [1], [2]]);
		expect(wrapper.destroyed).toBeTruthy();
	});

});
