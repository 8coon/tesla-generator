const Filter = require('../src/filter');
const EventBus = require('../src/event-bus');
const handlers = require('../src/handlers');
const effects = require('../src/effects');

const noopGenerator = function *() {};

const noopEvent = {type: 'noopEvent'};

class EventBusMock {
	constructor() {
		this.run = jest.fn();
		this.continueRunning = jest.fn();
		this.addFilter = jest.fn();
		this.removeFilter = jest.fn();
	}
}

describe('Handlers', () => {
	let eventBus;

	beforeEach(() => {
		eventBus = new EventBusMock();
	});

	test('"takeEvery" runs a new generator on every match', () => {
		const effect = effects.takeEvery('*', noopGenerator);

		// Simulating effect handling
		const shouldContinue = handlers[effects.EFFECT_TAKE_EVERY](
			eventBus, effect, noopGenerator
		);

		// takeEvery is a non-blocking effect
		expect(shouldContinue).toBeTruthy();

		// Expecting takeEvery to put filter
		expect(eventBus.addFilter.mock.calls).toEqual([[
			expect.objectContaining(
				new Filter('*', expect.any(Function))
			),
		]]);

		// Simulating condition match
		eventBus.addFilter.mock.calls[0][0].handleMatch(noopEvent);

		// Expecting takeEvery to run a new generator
		expect(eventBus.run.mock.calls).toEqual([
			[noopGenerator, noopEvent],
		]);
	});

	test('"takeEvery" accepts multiple matchers', () => {
		const effect = effects.takeEvery(['a', 'b'], noopGenerator);

		// Simulating effect handling
		handlers[effects.EFFECT_TAKE_EVERY](eventBus, effect, noopGenerator);

		// Expecting takeEvery to put filter to all events
		expect(eventBus.addFilter.mock.calls).toEqual(
			expect.arrayContaining(
				[[
					expect.objectContaining(
						new Filter('a', expect.any(Function)),
					),
				]],
			),
		);

		expect(eventBus.addFilter.mock.calls).toEqual(
			expect.arrayContaining(
				[[
					expect.objectContaining(
						new Filter('b', expect.any(Function)),
					),
				]],
			),
		);

		expect(eventBus.addFilter.mock.calls.length).toBe(2);
	});

	test('"take" continues to run a generator on match', () => {
		const effect = effects.take('*');
		// This effect should produce a filter like that
		const referenceFilter = expect.objectContaining(
			new Filter('*', expect.any(Function))
		);

		// Simulating effect handling
		const shouldContinue = handlers[effects.EFFECT_TAKE](
			eventBus, effect, noopGenerator
		);

		// take is a blocking effect
		expect(shouldContinue).toBeFalsy();

		// Expecting take to put filter
		expect(eventBus.addFilter.mock.calls).toEqual([[
			referenceFilter,
		]]);

		// No filters are expected to be removed at this point
		expect(eventBus.removeFilter.mock.calls.length).toBe(0);

		// Simulating condition match
		eventBus.addFilter.mock.calls[0][0].handleMatch(noopEvent);

		// Expecting take to continue running a generator
		expect(eventBus.continueRunning.mock.calls).toEqual([
			[noopGenerator, noopEvent],
		]);

		// Expecting take to remove filter
		expect(eventBus.removeFilter.mock.calls).toEqual([[
			referenceFilter,
		]]);

		// Expecting take to put and remove the same filter
		expect(eventBus.addFilter.mock.calls[0][0]).toBe(
			eventBus.removeFilter.mock.calls[0][0]
		);
	});

	test('"take" accepts multiple matchers', () => {
		const effect = effects.take(['a', 'b'], noopGenerator);

		// This effect should produce these filters
		const filterA = expect.objectContaining(
			new Filter('a', expect.any(Function)),
		);

		const filterB = expect.objectContaining(
			new Filter('b', expect.any(Function)),
		);

		// Simulating effect handling
		handlers[effects.EFFECT_TAKE](eventBus, effect, noopGenerator);

		// Expecting take to put filter to all events
		expect(eventBus.addFilter.mock.calls).toEqual(
			expect.arrayContaining([[filterA]]),
		);

		expect(eventBus.addFilter.mock.calls).toEqual(
			expect.arrayContaining([[filterB]]),
		);

		// No filters are expected to be removed at this point
		expect(eventBus.removeFilter.mock.calls.length).toBe(0);

		expect(eventBus.addFilter.mock.calls.length).toBe(2);

		// Simulating one condition match
		eventBus.addFilter.mock.calls[0][0].handleMatch(noopEvent);

		// Expecting take to remove filters to all events
		expect(eventBus.removeFilter.mock.calls).toEqual(
			expect.arrayContaining([[filterA]]),
		);

		expect(eventBus.removeFilter.mock.calls).toEqual(
			expect.arrayContaining([[filterB]]),
		);

		expect(eventBus.removeFilter.mock.calls.length).toBe(2);
	});

	test('"race" as array should block execution until at least one effect is resolved', () => {
		const eventBus = new EventBus();
		const fn = jest.fn();

		// Root generator
		eventBus.run(function *() {
			fn(0);

			const result = yield effects.race([
				effects.take('a'),
				effects.take('b'),
				effects.take('c'),
			]);

			fn(1);
			expect(result).toEqual([void 0, {type: 'b'}, void 0]);
		});

		// Expect generator execution to be blocked
		expect(fn.mock.calls).toEqual([[0]]);

		// Dispatch a non-matching event
		eventBus.dispatch({type: 'd'});

		// Expect generator execution to still be blocked
		expect(fn.mock.calls).toEqual([[0]]);

		// Dispatch a matching event
		eventBus.dispatch({type: 'b'});

		// Expect generator execution to continue
		expect(fn.mock.calls).toEqual([[0], [1]]);
	});

	test('"race" as object should block execution until at least one effect is resolved', () => {
		const eventBus = new EventBus();
		const fn = jest.fn();

		// Root generator
		eventBus.run(function *() {
			fn(0);

			const result = yield effects.race({
				a: effects.take('a'),
				b: effects.take('b'),
				c: effects.take('c'),
			});

			fn(1);
			expect(result).toEqual({b: {type: 'b'}});
		});

		// Expect generator execution to be blocked
		expect(fn.mock.calls).toEqual([[0]]);

		// Dispatch a non-matching event
		eventBus.dispatch({type: 'd'});

		// Expect generator execution to still be blocked
		expect(fn.mock.calls).toEqual([[0]]);

		// Dispatch a matching event
		eventBus.dispatch({type: 'b'});

		// Expect generator execution to continue
		expect(fn.mock.calls).toEqual([[0], [1]]);
	});

});
